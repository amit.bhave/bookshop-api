package com.tw.bootcamp.bookshop.bookshopapi.model;

public class BookShop {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
