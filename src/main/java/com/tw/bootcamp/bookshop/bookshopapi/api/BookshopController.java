package com.tw.bootcamp.bookshop.bookshopapi.api;

import com.tw.bootcamp.bookshop.bookshopapi.model.BookShop;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookshopController {

    @GetMapping("/application")
    public ResponseEntity<BookShop> applicationName(){
        BookShop bookShop = new BookShop();
        bookShop.setName("Sadda Bookstore");
        return new ResponseEntity<>(bookShop, HttpStatus.OK);
    }

}
