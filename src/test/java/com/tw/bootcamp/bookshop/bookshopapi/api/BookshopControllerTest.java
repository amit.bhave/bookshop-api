package com.tw.bootcamp.bookshop.bookshopapi.api;

import com.tw.bootcamp.bookshop.bookshopapi.model.BookShop;
import org.junit.Assert;
import org.junit.Test;

public class BookshopControllerTest {

    private BookshopController bookController;

    @Test
    public void applicationName() {
        bookController = new BookshopController();
        BookShop expectedBookShop = new BookShop();
        expectedBookShop.setName("Sadda Bookstore");
        Assert.assertEquals(expectedBookShop.getName(), ((BookShop)bookController.applicationName().getBody()).getName());
    }
}