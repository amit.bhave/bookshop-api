package com.tw.bootcamp.bookshop.bookshopapi.api;

import com.tw.bootcamp.bookshop.bookshopapi.model.BookShop;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BookshopControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void retrieveDetailsForCourse() throws Exception {
        mockMvc.perform(get("/application"))
                .andExpect(status().isOk())
                .andExpect(content().json("{'name':'Sadda Bookstore'}"))
                .andExpect(jsonPath("$.name").value("Sadda Bookstore"));
    }

}
